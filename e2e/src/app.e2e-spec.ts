import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;
  const testUser = {
    fullName: 'Jhon Doe',
    email: 'jhondoe@test.com',
    password: 123456
  };



  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo('/');
    expect(page.getTitleText()).toEqual('App is running!');
  });


  /* page content test after routing */
  it('should display home title message after routing', () => {
    page.getNavLink('/home').click();
    expect(page.getHomeTitleText()).toEqual('This is home page');
  });

  it('should display about title message after routing', () => {
    page.getNavLink('/about').click();
    expect(page.getAboutTitleText()).toEqual('This is about page');
  });

  it('should display contact title message after routing', () => {
    page.getNavLink('/contact').click();
    expect(page.getContactTitleText()).toEqual('Contact Form');
  });
  /* page content test after routing */


  /* nav link test */
  it('should display home button', () => {
    page.navigateTo('/');
    expect(page.getNavLink('/home').getText()).toEqual('Home');
  });

  it('should display about button', () => {
    page.navigateTo('/');
    expect(page.getNavLink('/about').getText()).toEqual('About');
  });

  it('should display contact button', () => {
    page.navigateTo('/');
    expect(page.getNavLink('/contact').getText()).toEqual('Contact');
  });
  /* nav link test */




  it('should signup', () => {
    page.navigateTo('/signup');
    page.getAndSetFullName('#fname', testUser.fullName);
    page.getAndSetEmail('#email', testUser.email);
    page.getAndSetPassword('#pass', testUser.password);
    page.signupSubmit().click();
    // expect(page.getNavLink('/contact').getText()).toEqual('Contact');
  });


  it('should login', () => {
    page.navigateTo('/login');
    page.getAndSetEmail('#email', testUser.email);
    page.getAndSetPassword('#pass', testUser.password);
    page.loginSubmit().click();
    // expect(page.getNavLink('/contact').getText()).toEqual('Contact');
  });


  it('check logout link', () => {
    page.navigateTo('/');
    expect(page.getLogoutLink().getText()).toEqual('Logout');
  });

  it('check email id print', () => {
    page.navigateTo('/');
    expect(page.getEmailIdPrint().getText()).toEqual(testUser.email);
  });


  it('check logout a user', () => {
    page.navigateTo('/');
    page.getLogoutLink().click();
  });

  it('check email id not print after logout', () => {
    page.navigateTo('/');
    expect (page.getEmailIdPrint().isPresent()).toBeFalsy();
  });




  // afterEach(async () => {
  //   // Assert that there are no errors emitted from the browser
  //   const logs = await browser.manage().logs().get(logging.Type.BROWSER);
  //   expect(logs).not.toContain(jasmine.objectContaining({
  //     level: logging.Level.SEVERE,
  //   } as logging.Entry));
  // });
});
