import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(path) {
    // return browser.get(browser.baseUrl) as Promise<any>;
    return browser.get(path);
  }

  getTitleText() {
    return element(by.css('app-root .content span')).getText() as Promise<string>;
  }

  getHomeTitleText() {
    return element(by.css('.outer-container h1')).getText() as Promise<string>;
  }

  getAboutTitleText() {
    return element(by.css('.outer-container h1')).getText() as Promise<string>;
  }

  getContactTitleText() {
    return element(by.css('.outer-container h1')).getText() as Promise<string>;
  }

  getNavLink(path) {
    return element(by.css('.nav-link[href="'+path+'"]'));
  }

  getAndSetFullName(id, val) {
    return element(by.css(id)).sendKeys(val);
  }

  getAndSetEmail(id, val) {
    return element(by.css(id)).sendKeys(val);
  }

  getAndSetPassword(id, val) {
    return element(by.css(id)).sendKeys(val);
  }

  signupSubmit() {
    return element(by.css('#signup-btn'));
  }

  loginSubmit() {
    return element(by.css('#login-btn'));
  }

  getLogoutLink() {
    return element(by.css('.nav-link.logout'));
  }

  getEmailIdPrint() {
    return element(by.css('.nav-link.email span'));
  }


}
