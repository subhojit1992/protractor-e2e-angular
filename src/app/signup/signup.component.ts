import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  user = {
    name: '',
    email: '',
    password: ''
  };

  constructor(private router: Router) { }



  onSubmit() {
    let allUser = JSON.parse(localStorage.getItem('protractor-users-data'));
    allUser.push(this.user);

    let allUserStringify = JSON.stringify(allUser);
    localStorage.setItem('protractor-users-data', allUserStringify);

    this.router.navigate(['/login']);
  }




  ngOnInit() {
  }

}
