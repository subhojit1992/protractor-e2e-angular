import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { DataService } from "./data.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'protractor-test';
  email = '';

  isLogin = false;

  constructor(private router: Router, private data: DataService) {
    if (localStorage.getItem('protractor-users-data') == null) {
      localStorage.setItem('protractor-users-data', '[]');
    }


    if (localStorage.getItem('protractor-user') == null || localStorage.getItem('protractor-user') == '') {
      this.data.changeAuthSource("notLoggedIn");
      this.email = '';
      this.isLogin = false;
    } else {
      this.data.changeAuthSource("isLoggedIn");
      this.email = JSON.parse(localStorage.getItem('protractor-user')).email;
      this.isLogin = true;
    }

  }



  logout() {
    localStorage.setItem('protractor-user', '');

    this.data.changeAuthSource("notLoggedIn");

    this.router.navigate(['/']);
  }


  ngOnInit() {

    this.data.currentAuthSource.subscribe(message => {
      if (message == 'notLoggedIn') {
        this.isLogin = false;
      } else {
        this.isLogin = true;
      }
    });

  }

}
