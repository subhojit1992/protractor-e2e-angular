import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {

  private authSource = new BehaviorSubject('notLoggedIn');
  currentAuthSource = this.authSource.asObservable();

  constructor() { }

  changeAuthSource(message: string) {
    this.authSource.next(message);
  }

}
