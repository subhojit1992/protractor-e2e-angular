import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { DataService } from "../data.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user = {
    email: '',
    password: ''
  };


  constructor(private router: Router, private data: DataService) { }


  onSubmit() {
    let allUser = JSON.parse(localStorage.getItem('protractor-users-data'));

    if (allUser.filter((item) => item.email === this.user.email && item.password == this.user.password).length === 1) {
      let user = JSON.stringify(this.user);
      localStorage.setItem('protractor-user', user);

      this.data.changeAuthSource("isLoggedIn")

      this.router.navigate(['/']);
    } else {
      alert('User not exist');
    }
  }


  ngOnInit() {
  }

}
